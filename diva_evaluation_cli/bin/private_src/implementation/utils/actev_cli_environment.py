"""Utils module

Can be used by any other module of the CLI
Implements functions to store a bash environment between commands
"""

import logging
import os

from dotenv import load_dotenv

current_dir = os.path.dirname(os.path.realpath(__file__))
ACTEV_CLI_ENVIRONMENT_FILEPATH = os.path.join(
    current_dir, 'actev_cli_environment.env')


def export_variable(name, value):
    '''Increment the bash environment file with a key and a value
    '''
    append_environment('export {}="{}";\n'.format(name, value))


def append_environment(content=''):
    '''Increment the bash environment file with some str content.
    '''
    init_environment(content, mode='a')


def init_environment(content='', mode='w'):
    '''Overwrite or create the bash environment file with some str content.
    '''
    with open(ACTEV_CLI_ENVIRONMENT_FILEPATH, mode) as env_file:
        env_file.write(content)


def activate_environment():
    '''Loads the environment variables from ACTEV_CLI_ENVIRONMENT_FILEPATH
    into the current environment.
    '''
    load_dotenv(dotenv_path=ACTEV_CLI_ENVIRONMENT_FILEPATH)
