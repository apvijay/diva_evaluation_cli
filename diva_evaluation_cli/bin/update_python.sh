#!/bin/bash

bin_dir="/usr/bin"
if [ ! -f $bin_dir/python3.7 ];then
  sudo apt install -y python3.7
fi
for py_v in $bin_dir/python $bin_dir/python3
do
  if [ -f $py_v ];then
    sudo rm -rf $py_v
  fi
  sudo ln -s $bin_dir/python3.7 $py_v
done
