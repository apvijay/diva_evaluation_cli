CLI commands documentation
==========================


* [actev design-chunks](actev_design_chunks.md)
* [actev exec](actev_exec.md)
* [actev experiment-init](actev_experiment_init.md)
* [actev experiment-cleanup](actev_experiment_cleanup.md)
* [actev get-system](actev_get_system.md)
* [actev merge-chunks](actev_merge_chunks.md)
* [actev post-process-chunk](actev_post_process_chunk.md)
* [actev pre-process-chunk](actev_pre_process_chunk.md)
* [actev process-chunk](actev_process_chunk.md)
* [actev reset-chunk](actev_reset_chunk.md)
* [actev status](actev_status.md)
* [actev system-setup](actev_system_setup.md)
* [actev validate-execution](actev_validate_execution.md)
* [actev validate-system](actev_validate_system.md)


