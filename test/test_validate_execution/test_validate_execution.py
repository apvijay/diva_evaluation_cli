'''
This module implements unit tests of the actev validate-execution command.
'''

import sys
import os
import unittest

from test import test_cli

cdir = os.path.dirname(os.path.abspath(__file__))
res = os.path.join(cdir, "resources")


class TestValidateExecution(unittest.TestCase):
    """
    Test the actev experiment-init command.
    """

    def setUp(self):
        self.output = os.path.join(res, "output.json")
        self.reference = os.path.join(res, "reference.json")
        self.file_index = os.path.join(res, "file-index.json")
        self.activity_index = os.path.join(res, "activity-index.json")
        self.result = os.path.join(res, "result")

    def test_validate_execution(self):
        sys.argv[1:] = ["validate-execution", "-f", self.file_index, "-a",
                        self.activity_index, "-o", self.output]
        test_cli('`actev experiment-init` failed.')

    def test_experiment_init_config(self):
        sys.argv[1:] = ["validate-execution", "-f", self.file_index, "-a",
                        self.activity_index, "-o", self.output, "-R",
                        self.result, "-r", self.reference, "--score"]
        test_cli('`actev validate-execution` with `--score` failed.')


if __name__ == '__main__':
    unittest.main()
